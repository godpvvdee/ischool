import { useContext } from "react";
import { AuthContext } from "@/contexts/AuthContext"; // ✅ Correct import

export const useAuth = () => {
  return useContext(AuthContext);
};
