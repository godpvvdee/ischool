/**
 * Validate email format
 */
export const isValidEmail = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };
  
  /**
   * Validate password (at least 6 characters)
   */
  export const isValidPassword = (password) => {
    return password.length >= 6;
  };
  